<!DOCTYPE html>

<html lang="en">
<head>
	<title>Soham Labels | Labeling & Printing Solutions</title>
	<?php
		include('head.php');
	?>
</head>
<body>

<div class="outer-wrapper">

	<!-- TOPBAR -->
	<?php
	include('navbar.php');
	?>
	<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="soham-labels-homepage" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;height: 550px;">
<!-- START REVOLUTION SLIDER 5.4.8.3 auto mode -->
	<div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.8.3">
<ul>	<!-- SLIDE  -->
	<li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="images/slider/slider-1.png"  alt="" title="slider-1"  width="1350" height="550" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->
	</li>
</ul>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
</div><!-- END REVOLUTION SLIDER -->

	<!-- SLIDER -->
	

	<!-- INNER CONTENT -->
	<div class="inner-content no-padding">
		<div class="container">
			<div class="space60"></div>
			<div class="welcome-content text-center">
				<h2>Welcome to Soham Labels</h2>
				<p class="lead">The best Printing & Labeling solution you are looking for</p>
			</div>
		</div>
		<div class="container">
			<div class="service-home col-md-12 text-center">
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="">
					<i class="fa fa-tags"></i>
					</a>
					<div class="clearfix"></div>
					<h4>Labeling Solution</h4>
					<p>We have got all the possible solutions for all your labeling needs be it industrial or commercial use.</p>
					<a class="readmore" href="#">Read more...</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="">
					<i class="icon-printer"></i>
					</a>
					<div class="clearfix"></div>
					<h4>Printing Solution</h4>
					<p>From labels printing to bills printing we will support your back at all the times.</p>
					<a class="readmore" href="#">Read more...</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="">
					<i class="fa fa-barcode"></i>
					</a>
					<div class="clearfix"></div>
					<h4>Barcode Solution</h4>
					<p>We wide range of barcode printers and scanners to all your requirements. We also provide barcode printing rolls.</p>
					<a class="readmore" href="#">Read more...</a>
				</div>
			</div>
		</div>
		
		<div class="clearfix"></div>
		
		<div class="container info-home">
			<div class="col-md-5">
				<img src="images/other/homepage-box-image.png" alt="">
			</div>
			<div class="col-md-7">
				<h3>Unlimited layouts can be created<em>A4 Label Sheet</em></h3>
				<p>Soham Labels offers wide range of labeling options with various size options. We provide A4 label sheet with varoius label sizes like 1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 21, 24, 30, 33, 40, 48, 56, 65, 84 in one Label Sheet. In addition to that A3, 12x18 and FS sizes are available.</p>
				<div class="clearfix space10"></div>
				<a href="contact-us.php" class="button btn-lg">Buy Now!</a>
			</div>
		</div>
		
		<div class="clearfix space90"></div>
		
		<section id="portfolio-section">
			<div class="container">
				<h3 class="uppercase text-center">Product Showcase</h3>
			</div>
			<div class="space20"></div>
			
			<div id="portfolio-home" class="isotope">
				<div class="project-item photography branding">
					<div class="project-gal">
						<img src="images/projects/1.png" class="img-responsive" alt="">
						<div class="overlay-folio">
							<div class="hover-box">
								<div class="hover-zoom">
									<a class="mp-lightbox zoom" href="images/projects/1.png"><i class="icon-plus2"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="project-info">
						<h2>Medical Industry.</h2>
						<p>Packaging</p>
					</div>
				</div>
				<div class="project-item illustration web-design illustration">
					<div class="project-gal">
						<img src="images/projects/2.png" class="img-responsive" alt="">
						<div class="overlay-folio">
							<div class="hover-box">
								<div class="hover-zoom">
									<a class="mp-lightbox zoom" href="images/projects/2.png"><i class="icon-plus2"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="project-info">
						<h2>Packaging for Commercials</h2>
						<p>Packaging</p>
					</div>
				</div>
				<div class="project-item illustration print">
					<div class="project-gal">
						<img src="images/projects/6.png" class="img-responsive" alt="">
						<div class="overlay-folio">
							<div class="hover-box">
								<div class="hover-zoom">
									<a class="mp-lightbox zoom" href="images/projects/6.png"><i class="icon-plus2"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="project-info">
						<h2>Shipping Labels</h2>
						<p>Shipping Box</p>
					</div>
				</div>
				<div class="project-item photography web-design">
					<div class="project-gal">
						<img src="images/projects/4.png" class="img-responsive" alt="">
						<div class="overlay-folio">
							<div class="hover-box">
								<div class="hover-zoom">
									<a class="mp-lightbox zoom" href="images/projects/4.png"><i class="icon-plus2"></i></a>			
								</div>
							</div>
						</div>
					</div>
					<div class="project-info">
						<h2>Courier Boxes</h2>
						<p>Shipping Box</p>
					</div>
				</div>
				<div class="project-item branding">
					<div class="project-gal">
						<img src="images/projects/5.png" class="img-responsive" alt="">
						<div class="overlay-folio">
							<div class="hover-box">
								<div class="hover-zoom">
									<a class="mp-lightbox zoom" href="images/projects/5.png"><i class="icon-plus2"></i></a>			
								</div>
							</div>
						</div>
					</div>
					<div class="project-info">
						<h2>Label for Medicine</h2>
						<p>Packaging</p>
					</div>
				</div>
				
			</div>
		</section>
		

		<section id="others-section">
			<div class="container">
				<h2 class="uppercase text-center space30">Other Products</h2>
				<div class="space20"></div>
			<ul class="gal-3col">
				<li><a class="mp-lightbox" href="images/projects/cash-recipt-printers.png"><img src="images/projects/cash-recipt-printers.png" class="img-responsive" alt=""></a></li>
				<li><a class="mp-lightbox" href="images/projects/label-prints.png"><img src="images/projects/label-prints.png" class="img-responsive" alt=""></a></li>
				<li><a class="mp-lightbox" href="images/projects/barcode-labels.png"><img src="images/projects/barcode-labels.png" class="img-responsive" alt=""></a></li>
				<li><a class="mp-lightbox" href="images/projects/barcode-scanner.png"><img src="images/projects/barcode-scanner.png" class="img-responsive" alt=""></a></li>
				<li><a class="mp-lightbox" href="images/projects/printed-label.png"><img src="images/projects/printed-label.png" class="img-responsive" alt=""></a></li>
				<li><a class="mp-lightbox" href="images/projects/rol-labels.png"><img src="images/projects/rol-labels.png" class="img-responsive" alt=""></a></li>
			</ul>
			</div>

		</section>

		<div class="container padding70">
			<div class="text-center space40">
				<h2 class="title uppercase">Why Choose Soham Labels?</h2>
				<p>Here are some of the winning factors to choose soham labels over other options in market.</p>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="space90"></div>
					<ul class="features-left">
						<li>
							<i class="fa fa-refresh"></i>
							<h3>Consistant Supply</h3>
							<p> We never let our customers get down by proving inconsistant supply.</p>
						</li>
						<li>
							<i class="fa fa-rupee"></i>
							<h3>Resoanable Price</h3>
							<p> We offer very reasonable prices compare to other market competiters</p>
						</li>
						<li>
							<i class="fa fa-trophy"></i>
							<h3>Topnotch Quality </h3>
							<p> Soham Labels never compromise with quality</p>
						</li>
					</ul>
				</div>
				<div class="col-sm-4 col-sm-push-4">
					<div class="space90"></div>
					<ul class="features-right">
						<li>
							<i class="fa fa-lightbulb-o"></i>
							<h3>Multipurpose Solution</h3>
							<p> We have solutions for personal and industry needs.</p>
						</li>
						<li>
							<i class="fa fa-support"></i>
							<h3>Always there for you</h3>
							<p> We help our customers getting proper service by availing them quick support</p>
						</li>
						<li>
							<i class="fa fa-user"></i>
							<h3>Satisfied Customers</h3>
							<p> For the years now we have many satisfied customers in various industries</p>
						</li>
					</ul>
				</div>
				<div class="col-sm-4 col-sm-pull-4">
					<div class="space90"></div>
					<div> <img src="images/other/why-soham-labels.png" class="img-responsive center-block" alt=""/> </div>
				</div>
			</div>
		</div>
		
		
	</div>

	<div class="parallax-bg3 padding70">
		<div class="container home-testimonials ">
			<div class="text-center space70">
				<h2 class="white uppercase">What our customers Say</h2>
			</div>
			<div id="home-quote" class="owl-carousel owl-theme">
				<div class="item">
					<div class="hq-info">
						<p>" Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years.</p>
						<img src="images/quote/thumb/1.png" alt="">
						<span><strong>Michile Johnson</strong> manager - websitename.com</span>
					</div>
				</div>
				<div class="item">
					<div class="hq-info">
						<p>" Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years.</p>
						<img src="images/quote/thumb/2.png" alt="">
						<span><strong>Michile Johnson</strong> manager - websitename.com</span>
					</div>
				</div>
				<div class="item">
					<div class="hq-info">
						<p>" Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years.</p>
						<img src="images/quote/thumb/3.png" alt="">
						<span><strong>Michile Johnson</strong> manager - websitename.com</span>
					</div>
				</div>
				<div class="item">
					<div class="hq-info">
						<p>" Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years.</p>
						<img src="images/quote/thumb/4.png" alt="">
						<span><strong>Michile Johnson</strong> manager - websitename.com</span>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
include('footer.php');
?>
</div>

<!-- STYLE SWITCHER 
============================================= -->

<!-- END STYLE SWITCHER 
============================================= -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Plugins -->
<script src="js/bootstrap.min.js"></script>
<script src="js/menu.js"></script>
<script src="js/owl-carousel/owl.carousel.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/isotope/isotope.pkgd.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<!-- <script src="js/tweecool.js"></script> -->
<script src="js/flexslider/jquery.flexslider.js"></script>
<script src="js/easypie/jquery.easypiechart.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.inview.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/magnific-popup/jquery.magnific-popup.min.js"></script>



<script src="js/main.js"></script>

<script type="text/javascript">
var revapi1,
	tpj;	
(function() {			
	if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded",onLoad); else onLoad();	
	function onLoad() {				
		if (tpj===undefined) { tpj = jQuery; if("off" == "on") tpj.noConflict();}
	if(tpj("#rev_slider_1_1").revolution == undefined){
		revslider_showDoubleJqueryError("#rev_slider_1_1");
	}else{
		revapi1 = tpj("#rev_slider_1_1").show().revolution({
			sliderType:"standard",
			jsFileLocation:"//localhost/_projects/test-wordpress/wp-content/plugins/revslider/public/assets/js/",
			sliderLayout:"auto",
			dottedOverlay:"none",
			delay:9000,
			navigation: {
				onHoverStop:"off",
			},
			visibilityLevels:[1240,1024,778,480],
			gridwidth:1240,
			gridheight:550,
			lazyType:"none",
			shadow:0,
			spinner:"spinner0",
			stopLoop:"off",
			stopAfterLoops:-1,
			stopAtSlide:-1,
			shuffle:"off",
			autoHeight:"off",
			disableProgressBar:"on",
			hideThumbsOnMobile:"off",
			hideSliderAtLimit:0,
			hideCaptionAtLimit:0,
			hideAllCaptionAtLilmit:0,
			debugMode:false,
			fallbacks: {
				simplifyAll:"off",
				nextSlideOnWindowFocus:"off",
				disableFocusListener:false,
			}
		});
	}; /* END OF revapi call */
	
 }; /* END OF ON LOAD FUNCTION */
}()); /* END OF WRAPPING FUNCTION */
</script>
 
</body>
</html>

