<!DOCTYPE html>
<html lang="en">
<head>
	<title>404 Not Found | Soham Labels</title>
	<?php 
		include ('head.php');
	?>
</head>
<body id="errorpage">

<div class="outer-wrapper">

	<!-- TOPBAR -->
	<?php
	include('navbar.php');
	?>

	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="page_header_parallax2">
			<div class="container">
				<div class="col-md-12">
					<h3 class="text-center">Page not found</h3>
				</div>
			</div>
		</div>
	</div>

	<!-- 404 ERROR CONTENT -->
	<div class="inner-content">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center error-404">
					<h2>404</h2>
					<p>Error 404! Sorry, the page you requested was not found.</p>
					<div class="clearfix"></div>
					<a href="/" class="button btn-center"><i class="icon-circle-arrow-left"></i>Back to Home</a>
				</div>
			</div>
		</div>
	</div>

	<?php include ('footer.php'); ?>
</div>

<!-- STYLE SWITCHER 
============================================= -->

<!-- END STYLE SWITCHER 
============================================= -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Plugins -->
<script src="js/bootstrap.min.js"></script>
<script src="js/menu.js"></script>
<script src="js/owl-carousel/owl.carousel.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/isotope/isotope.pkgd.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/tweecool.js"></script>
<script src="js/flexslider/jquery.flexslider.js"></script>
<script src="js/easypie/jquery.easypiechart.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.inview.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>
