
<!-- TWEET / SUBSCRIBE -->
	

	<!-- FOOTER -->
	<footer>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-105796732-9"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-105796732-9');
		</script>

		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/5ccae41fd07d7e0c639194a4/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
		<!--End of Tawk.to Script-->

		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h4 class="space30">About us</h4>
			    
					<p>Soham Labels PVT LTD, since 2010, is a Printing and Barcode solution and services provider with a strong focus in the area of Digital Print Media such as Sticker Printing, Labels Printing (Self Stick & Non-Stick both) etc.</p>
				</div>
				<div class="col-md-1">
					
				</div>
				<div class="col-md-3">
					<h4 class="space30">Quick Links</h4>
					<ul class="list-style1">
						<li>
							<a href="index.php"><i class="icon-play"></i> Home </a>
						</li>
						<li>
							<a href="products.php"><i class="icon-play"></i> Services </a>
						</li>
						<li>
							<a href="#"><i class="icon-play"></i> Contact </a>
						</li>
						<li>
							<a href="#"><i class="icon-play"></i> Privacy Policy </a>
						</li>
						<li>
							<a href="#"><i class="icon-play"></i> Terms of Use </a>
						</li>
					</ul>
					
				</div>
				<div class="col-md-4">
					<h4 class="space30">Contact</h4>
			  		<ul class="c-info">
						<li><i class="fa fa-map-marker"></i> Plot No 69, A-2, Block No 59, Near Dairy Don Factory, Moti Naroli, Ta: Mangrol, Kim, Surat - 394110, Gujarat
						</li>
						<li>
						</li>
						<li><i class="fa fa-phone"></i> +91 9601319607 </li>
						<li><i class="fa fa-envelope-o"></i> sohamlabels@gmail.com </li>
					</ul>
					
				</div>
			</div>
		</div>
	</footer>

	<!-- FOOTER COPYRIGHT -->
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<p>&copy; Copyright 2010 - <?php echo date("Y"); ?>. Soham Labels.
				</div>
				<div class="col-md-4">
					<div class="f-social pull-right">
						<a class="fa fa-facebook" href="http://www.facebook.com"></a>
						<a class="fa fa-twitter" href="http://www.twitter.com"></a>
						<a class="fa fa-instagram" href="http://www.instagram.com"></a>
					</div>
				</div>
			</div>
		</div>
	</div>