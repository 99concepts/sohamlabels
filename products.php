<!DOCTYPE html>
<html lang="en">
<head>
  
	<title>Services | Soham Labels</title>
	<?php include ('head.php'); ?>

</head>
<body>

<div class="outer-wrapper">

	<!-- TOPBAR -->
	<?php include ('navbar.php'); ?>

	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="bcrumb-wrap">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="bcrumbs">
							<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
							<li>Products</li>
						</ul>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- INNER CONTENT -->
	<div class="inner-content no-padding">
		<div class="container">
			<section id="portfolio-section">
				<div id="portfolio-home" class="isotope gutter folio-boxed-3col" style="position: relative; height: 743.532px;">
					<div class="project-item photography branding" style="position: absolute; left: 0px; top: 0px;">
						<a href="#">
							<div class="project-gal">
								<img src="images/projects/blank-label-sheet.png" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Blank Label Sheets</h2>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="project-item illustration web-design illustration" style="position: absolute; left: 380px; top: 0px;">
						<a href="#">
							<div class="project-gal">
								<img src="images/projects/printed-label.png" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Printed Label Sheet</h2>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="project-item illustration print" style="position: absolute; left: 760px; top: 0px;">
						<a href="#">
							<div class="project-gal">
								<img src="images/projects/rol-labels.png" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Roll Labels</h2>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="project-item photography web-design" style="position: absolute; left: 0px; top: 247px;">
						<a href="#">
							<div class="project-gal">
								<img src="images/projects/barcode-labels.png" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Barcode Labels</h2>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="project-item branding" style="position: absolute; left: 380px; top: 247px;">
						<a href="#">
							<div class="project-gal">
								<img src="images/projects/barcode-scanner.png" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Barcode Scanner</h2>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="project-item illustration web-design print" style="position: absolute; left: 760px; top: 247px;">
						<a href="#">
							<div class="project-gal">
								<img src="images/projects/cash-recipt-printers.png" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Receipt Printer</h2>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="project-item photography branding illustration" style="position: absolute; left: 0px; top: 495px;">
						<a href="#">
							<div class="project-gal">
								<img src="images/projects/label-prints.png" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Label Printers</h2>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="project-item illustration web-design" style="position: absolute; left: 380px; top: 495px;">
						<a href="#">
							<div class="project-gal">
								<img src="images/projects/tap-labels.png" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Tape Labels</h2>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="project-item branding web-design print" style="position: absolute; left: 760px; top: 495px;">
						<a href="#">
							<div class="project-gal">
								<img src="images/projects/barcode-printer.png" class="img-responsive" alt="">
								<div class="overlay-folio2">
									<div class="project-info">
										<h2>Barcode Printers</h2>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
			</section>
		</div>
		
	</div>
	
	<!-- TWEET / SUBSCRIBE -->
	<?php include ('footer.php'); ?>
</div>


<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Plugins -->
<script src="js/bootstrap.min.js"></script>
<script src="js/menu.js"></script>
<script src="js/owl-carousel/owl.carousel.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/isotope/isotope.pkgd.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/tweecool.js"></script>
<script src="js/flexslider/jquery.flexslider.js"></script>
<script src="js/easypie/jquery.easypiechart.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.inview.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="js/main.js"></script>

</body>
</html>
