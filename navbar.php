<div id="top-bar" class="hidden-xs hidden-sm">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="top-bar-content">
					<p>Have any questions? <span></span><i class="icon-mobile2"></i> +91 9601319607 <span></span>&bull;<span></span> <i class="icon-mail3"></i> Mail us - <a href="mailto:sohamlabels@gmail.com">sohamlabels@gmail.com</a></p>
				</div>
				<div class="nav-social-icons">
					<ul class="social-icons">
						<li class="facebook">
							<a href="http://facebook.com" target="_blank">
								<i class="fa fa-facebook"></i><i class="fa fa-facebook"></i>
							</a>
						</li>
						<li class="twitter">
							<a href="http://twitter.com" target="_blank">
								<i class="fa fa-twitter"></i>
								<i class="fa fa-twitter"></i>
							</a>
						</li>
						<li class="instagram">
							<a href="http://instagram.com" target="_blank">
								<i class="fa fa-instagram"></i>
								<i class="fa fa-instagram"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- HEADER -->
<header id="header-main">
	<div class="container">
		<div class="navbar yamm navbar-default">
			<div class="navbar-header">
				<button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a href="index.php" class="navbar-brand"><img src="images/basic/logo.png" alt=""/></a>
			</div>
			<!-- CART / SEARCH -->
			
			<div id="navbar-collapse-1" class="navbar-collapse collapse navbar-right">
				<ul class="nav navbar-nav">
					<li class="dropdown yamm-fw">
						<a href="index.php">
							Home
						</a>
					</li>
					<li class="dropdown yamm-fw">
						<a href="about.php">
							About Us
						</a>
					</li>
					<li class="dropdown yamm-fw">
						<a href="products.php">
							Products
						</a>
					</li>
					<li class="dropdown">
						<a href="contact-us.php">
							Contact Us
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</header>