<!DOCTYPE html>
<html lang="en">
<head>
  
	<title>About | Soham Labels</title>

	<?php
		include('head.php');
	?>

</head>
<body>

<div class="outer-wrapper">

	<!-- TOPBAR -->
	<?php
		include('navbar.php');
	?>

	<!-- PAGE HEADER -->
	<div class="page_header">
		
		<div class="bcrumb-wrap">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="bcrumbs">
							<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
							<li>About</li>
						</ul>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="inner-content">
		<div class="container">

			<!-- ABOUT -->
			<div class="section-about space100">
				<div class="row">
					<!-- <div class="col-md-4">
						<div>
							<img src="images/team/neerav.png" class="img-responsive" alt="">
						</div>
					</div> -->
					<div class="col-md-12">
						<div>
							<h3>Who We Are &amp; What We Do</h3>
							<p style="font-size: 15px;">Soham Labels PVT LTD, since 2010, is a Printing and Barcode solution & services provider with a strong focus in the area of Digital Print Media such as Sticker Printing, Labels Printing (Self Stick & Non-Stick both) etc. Soham Labels’ goal of providing Single Source Total Solution is served by its strategic business units for hardware, software, consumables & support. By following this framework, we achieve a high Quality of Service and Better Customer delight. Over the years we have been serving many businesses with our topnotch quality printing solutions & services mainly in Ceramic, Pharma & Chemical industries.
							<br><br>
							We have state of the art technologies available at our factory located near Surat, Gujarat. Soham Labels is an organization which offers high-quality services, fast delivery & the competitive prices in the market. We have a dedicated team of people and the right attitude to match the ever-evolving requirements of our customers, both in relation to quality and quantity, without compromising on quality and being cost-efficient. Our team consists of experienced professionals having a dynamic & innovative approach. Our belief is to deliver high-quality products that promise a good price-performance ratio.
							<br><br>
							<strong>Mr. Vivek Kanjiya</strong> is the man behind all the operations and workings of Soham Labels. Under his vision and guidance Soham Group has become one of leading organization in various public sector industries across the country.

 </p>
						</div>
					</div>
				</div>
			</div>

			<!-- SERVICES -->
			<div class="section-services2 space40">
				<div class="row">
					<div class="col-md-6">
						<h4>Company's core values</h4>
						<ul class="list-style1 list-color2">
							<li><i class="icon-arrow-right2"></i>Positivity: Immediate help desk support as well as on call support for any query regarding our services and products. </li>
							<li><i class="icon-arrow-right2"></i>Creativity: We always find creative and unique solutions for every business’ problems to give best to our customers. </li>
							<li><i class="icon-arrow-right2"></i>Transparency: We have very transparent prices and solutions in comparison to other competitors in market. </li>
							<li><i class="icon-arrow-right2"></i>Passion: We have team of very passionate professionals working day & night to provide fruitful services to our clients. </li>
							<li><i class="icon-arrow-right2"></i>Consistency: Consistency is the best word to describe Soham Labels in one word. We give our work the first priority to provide consistent services to our clients.</li>
							<li><i class="icon-arrow-right2"></i>Efficiency: We are well known in the market for our efficient and quality products and services for years.</li>
						</ul>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6 ss2-content">
								
								<h4>Vision</h4>
								<p>Becoming the most admired company in printing solution industry known for its innovative solutions, world-class products, customer-centric operations, customer delight, and quality services.</p>
							</div>
							<!-- end section -->
							<div class="col-md-6 ss2-content">
								
								<h4>Mission</h4>
								<p>To help organizations growing & optimize their operations by providing the best available printing solutions that reduces cost, increases productivity, improves data processing and eliminates human errors both inside and outside the four walls of the organization.</p>
							</div>
							<!-- end section -->
						</div>
					</div>
					<!-- end section -->

				</div>
			</div>

			<div class="clearfix"></div>

			<!-- TESTIMONIALS -->
			<section id="section-testimonials">
				<div class="container">
					<h2 class="content-head text-center">Testimonials <em>Our industry experts help scale grow and succeed.</em></h2>
					<div class="testimonial-box">
						<div id="testimonial" class="owl-carousel">
							<div class="quote-info">
								<img src="images/quote/1.jpg" class="img-responsive" alt="">
								<p>Duis iaculis mauris dui tellus arcu rhoncus tellus non suscipit nisl tincidunt eget. Mauris in porta sapien. Pellentesque luctus urna volutpat, mollis dolor porttitor, rutrum sem. Aliquam vitae orci a libero iaculis sollicitudin. Sed ullamcorper lorem justo, ut elementum enim dapibus vel.</p>
								<h2>David Billie</h2>
							</div>
							<div class="quote-info">
								<img src="images/quote/2.jpg" class="img-responsive" alt="">
								<p>Duis iaculis mauris dui tellus arcu rhoncus tellus non suscipit nisl tincidunt eget. Mauris in porta sapien. Pellentesque luctus urna volutpat, mollis dolor porttitor, rutrum sem. Aliquam vitae orci a libero iaculis sollicitudin. Sed ullamcorper lorem justo, ut elementum enim dapibus vel.</p>
								<h2>Katey Thane</h2>
							</div>
							<div class="quote-info">
								<img src="images/quote/3.jpg" class="img-responsive" alt="">
								<p>Duis iaculis mauris dui tellus arcu rhoncus tellus non suscipit nisl tincidunt eget. Mauris in porta sapien. Pellentesque luctus urna volutpat, mollis dolor porttitor, rutrum sem. Aliquam vitae orci a libero iaculis sollicitudin. Sed ullamcorper lorem justo, ut elementum enim dapibus vel.</p>
								<h2>Wally Buddy</h2>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>

	<?php include ('footer.php'); ?>
</div>



<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Plugins -->
<script src="js/bootstrap.min.js"></script>
<script src="js/menu.js"></script>
<script src="js/owl-carousel/owl.carousel.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/isotope/isotope.pkgd.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/tweecool.js"></script>
<script src="js/flexslider/jquery.flexslider.js"></script>
<script src="js/easypie/jquery.easypiechart.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.inview.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="js/main.js"></script>

</body>
</html>