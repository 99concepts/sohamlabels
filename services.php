<!DOCTYPE html>
<html lang="en">
<head>
  
	<title>Services | Soham Labels</title>
	<?php include ('head.php'); ?>

</head>
<body>

<div class="outer-wrapper">

	<!-- TOPBAR -->
	<?php include ('navbar.php'); ?>

	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="bcrumb-wrap">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="bcrumbs">
							<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
							<li>Services</li>
						</ul>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- INNER CONTENT -->
	<div class="inner-content no-padding">
		<div class="container">
			<div class="services padding80">
				<div class="row">
					<div class="col-md-3">
						<div class="service-content text-center">
							<span><i class="icon-monitor"></i></span>
							<div class="services-content">
								<h2>Responsive Design</h2>
								<p>Curabitur eleifend leo justo id risus vel imperdiet justo a cursus risusauctor ullamcorper elit a feugiat.</p>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="service-content text-center">
							<span><i class="icon-cog3"></i></span>
							<div class="services-content">
								<h2>Fully Customizable</h2>
								<p>Curabitur eleifend leo justo id risus vel imperdiet justo a cursus risusauctor ullamcorper elit a feugiat.</p>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="service-content text-center">
							<span><i class="icon-layout"></i></span>
							<div class="services-content">
								<h2>unlimited Layouts</h2>
								<p>Curabitur eleifend leo justo id risus vel imperdiet justo a cursus risusauctor ullamcorper elit a feugiat.</p>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="service-content text-center">
							<span><i class="icon-support"></i></span>
							<div class="services-content">
								<h2>Live Support</h2>
								<p>Curabitur eleifend leo justo id risus vel imperdiet justo a cursus risusauctor ullamcorper elit a feugiat.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-8 col-md-offset-2 text-center">
				<h2>We Craft Digital Experiences.</h2>
				<p>Sed dapibus, leo ut placerat bibendum, ligula ligula consectetur eros, sed efficitur justo ex ut risus. Integer nec eros non elit finibus dictum quis sit amet augue.</p>
			</div>
			
			<div class="features">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div>
								<img src="images/other/1.jpg" class="img-responsive" alt="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="fc-main">
								<div class="feature-content space40">
									<i class="fa fa-hdd-o"></i>
									<div class="fc-inner">
										<h2>Website Design</h2>
										<p>Morbi vel imperdiet justo, a cursus risus. Sed auctor ullamcorper elit a feugiat. </p>
									</div>
								</div>
								<div class="feature-content space40">
									<i class="icon-pencil2"></i>
									<div class="fc-inner">
										<h2>Software Development</h2>
										<p>Morbi vel imperdiet justo, a cursus risus. Sed auctor ullamcorper elit a feugiat. </p>
									</div>
								</div>
								<div class="feature-content space40">
									<i class="icon-heart22"></i>
									<div class="fc-inner">
										<h2>Support System</h2>
										<p>Morbi vel imperdiet justo, a cursus risus. Sed auctor ullamcorper elit a feugiat. </p>
									</div>
								</div>
								<div class="feature-content space40">
									<i class="icon-heart22"></i>
									<div class="fc-inner">
										<h2>Support System</h2>
										<p>Morbi vel imperdiet justo, a cursus risus. Sed auctor ullamcorper elit a feugiat. </p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="pattern-bg1 padding40">
				<div class="cta-dark">
					<div class="col-md-12">
						<div class="col-md-9">
							<h3 class="white no-margin"><span>Welcome To trend</span> - Business HTML Template</h3>
							<p class="lite-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit mperdiet purus quis metus.</p>
						</div>
						<div class="col-md-3">
							<a class="button btn-lg color2 btn-radius pull-right">Buy Template</a>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		
		<div class="space70"></div>
		
		<div id="stats3" class="parallax-bg1">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6">
						<div class="stats3-info">
							<i class="icon-location3"></i>
							<p><span class="count">715</span></p>
							<h2>Places Visited</h2>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="stats3-info">
							<i class="icon-box2"></i>
							<p><span class="count">350</span></p>
							<h2>Box shadows</h2>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="stats3-info">
							<i class="icon-photoshop"></i>
							<p><span class="count">427</span></p>
							<h2>Vectors created</h2>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="stats3-info">
							<i class="icon-bookmark"></i>
							<p><span class="count">2540</span></p>
							<h2>Bookmarks</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="pricing-table3 pattern-bg2">
			<div class="container">
				<ul>
					<li>
						<h3>Standard</h3>
						<span class="price"> $0.99 <small>per month</small> </span>
						<ul class="benefits-list">
							<li>Responsive</li>
							<li>Documentation</li>
							<li class="not">Multiplatform</li>
							<li class="not">Video background</li>
							<li class="not">Support</li>
						</ul>
						<a href="#" target="_blank" class="buy"> <i class="icon-uniB7"></i></a>
					</li>
					<li>
						<h3>Silver</h3>
						<span class="price"> $3.99 <small>per month</small> </span>
						<ul>
							<li class="active">Responsive</li>
							<li class="active">Documentation</li>
							<li class="active">Multiplatform</li>
							<li class="not">Video background</li>
							<li class="not">Support</li>
						</ul>
						<a href="#" target="_blank" class="buy"> <i class="icon-uniB7"></i></a>
					</li>
					<li>
						<div class="stamp"><span><i class="icon icon-trophy"></i>Best choice</span></div>
						<h3>Gold</h3>
						<span class="price"> $8.99 <small>per month</small> </span>
						<ul>
							<li class="active">Responsive</li>
							<li class="active">Documentation</li>
							<li class="active">Multiplatform</li>
							<li class="active">Video background</li>
							<li class="not">Support</li>
						</ul>
						<a href="#" target="_blank" class="buy"> <i class="icon-uniB7"></i></a>
					</li>
					<li>
						<h3>Platinum</h3>
						<span class="price"> $12.99 <small>per month</small> </span>
						<ul>
							<li class="active">Responsive</li>
							<li class="active">Documentation</li>
							<li class="active">Multiplatform</li>
							<li class="active">Video background</li>
							<li class="active">Support</li>
						</ul>
						<a href="#" target="_blank" class="buy"> <i class="icon-uniB7"></i></a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
	<!-- TWEET / SUBSCRIBE -->
	<?php include ('footer.php'); ?>
</div>


<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Plugins -->
<script src="js/bootstrap.min.js"></script>
<script src="js/menu.js"></script>
<script src="js/owl-carousel/owl.carousel.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/isotope/isotope.pkgd.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/tweecool.js"></script>
<script src="js/flexslider/jquery.flexslider.js"></script>
<script src="js/easypie/jquery.easypiechart.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.inview.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="js/main.js"></script>

</body>
</html>
