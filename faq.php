<!DOCTYPE html>
<html lang="en">
<head>
  
	<title>FAQ | Soham Labels</title>
<?php include ('head.php'); ?>

</head>
<body>

<div class="outer-wrapper">

	<!-- TOPBAR -->
	<?php include ('navbar.php'); ?>

	<!-- PAGE HEADER -->
	<div class="page_header">
		<div class="page_header_parallax">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3><span>FAQ</span>Lorem ipsum dolor <br>amet iplus</h3>
					</div>
				</div>
			</div>
		</div>
		<div class="bcrumb-wrap">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="bcrumbs">
							<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
							<li>Faq</li>
						</ul>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- INNER CONTENT -->
	<div class="inner-content">
		<div class="container">
			<div class="block-heading">
				<h3><span>FAQ Style - #1</span></h3>
			</div>
			<div class="panel-group" id="accordion-alt1">
				<div class="panel faq-acc1">
					<!-- Panel heading -->
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-alt1" href="#collapseOne-alt1">
							1. Lorem ipsum dolor sit amet?
							</a>
						</h4>
					</div>
					<div style="height: 0px;" id="collapseOne-alt1" class="panel-collapse collapse">
						<!-- Panel body -->
						<div class="panel-body">
							Praesent tristique nisi in tortor placerat, non feugiat tellus pharetra. Mauris dignissim gravida odio, lobortis suscipit tellus egestas nec. Cras nunc odio, lacinia at feugiat ac, tincidunt ut ex. Donec dignissim sit amet velit sed pretium. Aenean at viverra neque. Nam congue lacinia massa, at sodales sem fringilla vitae. Integer ut pellentesque ante, non pellentesque augue. Nunc mollis odio non velit tincidunt, molestie congue erat consequat. 					
						</div>
					</div>
				</div>
				<div class="panel faq-acc1">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-alt1" href="#collapseTwo-alt1">
							2. What are dignissim gravida odio?
							</a>
						</h4>
					</div>
					<div style="height: 0px;" id="collapseTwo-alt1" class="panel-collapse collapse">
						<div class="panel-body">
							Praesent tristique nisi in tortor placerat, non feugiat tellus pharetra. Mauris dignissim gravida odio, lobortis suscipit tellus egestas nec. Cras nunc odio, lacinia at feugiat ac, tincidunt ut ex. Donec dignissim sit amet velit sed pretium. Aenean at viverra neque. Nam congue lacinia massa, at sodales sem fringilla vitae. Integer ut pellentesque ante, non pellentesque augue. Nunc mollis odio non velit tincidunt, molestie congue erat consequat. 					
						</div>
					</div>
				</div>
				<div class="panel faq-acc1">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-alt1" href="#collapseThree-alt1">
							3. Where can i metus eget risus tincidunt?
							</a>
						</h4>
					</div>
					<div style="height: 0px;" id="collapseThree-alt1" class="panel-collapse collapse">
						<div class="panel-body">
							Praesent tristique nisi in tortor placerat, non feugiat tellus pharetra. Mauris dignissim gravida odio, lobortis suscipit tellus egestas nec. Cras nunc odio, lacinia at feugiat ac, tincidunt ut ex. Donec dignissim sit amet velit sed pretium. Aenean at viverra neque. Nam congue lacinia massa, at sodales sem fringilla vitae. Integer ut pellentesque ante, non pellentesque augue. Nunc mollis odio non velit tincidunt, molestie congue erat consequat. 					
						</div>
					</div>
				</div>
				<div class="panel faq-acc1">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion-alt1" href="#collapseFour-alt1">
							4. Why the nec lectus sodales?
							</a>
						</h4>
					</div>
					<div style="height: 0px;" id="collapseFour-alt1" class="panel-collapse collapse">
						<div class="panel-body">
							Praesent tristique nisi in tortor placerat, non feugiat tellus pharetra. Mauris dignissim gravida odio, lobortis suscipit tellus egestas nec. Cras nunc odio, lacinia at feugiat ac, tincidunt ut ex. Donec dignissim sit amet velit sed pretium. Aenean at viverra neque. Nam congue lacinia massa, at sodales sem fringilla vitae. Integer ut pellentesque ante, non pellentesque augue. Nunc mollis odio non velit tincidunt, molestie congue erat consequat. 					
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix space60"></div>
			<div class="block-heading">
				<h3><span>FAQ Style - #2</span></h3>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="panel-group" id="accordion-alt2">
						<div class="panel faq-acc2">
							<!-- Panel heading -->
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion-alt2" href="#collapseOne-alt2">
									<i class="icon-help2"></i> What are dignissim gravida odio?
									</a>
								</h4>
							</div>
							<div style="height: 0px;" id="collapseOne-alt2" class="panel-collapse collapse">
								<!-- Panel body -->
								<div class="panel-body">
									Praesent tristique nisi in tortor placerat, non feugiat tellus pharetra. Mauris dignissim gravida odio, lobortis suscipit tellus egestas nec. Cras nunc odio, lacinia at feugiat ac, tincidunt ut ex. Donec dignissim sit amet velit sed pretium.							
								</div>
							</div>
						</div>
						<div class="panel faq-acc2">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion-alt2" href="#collapseTwo-alt2">
									<i class="icon-help2"></i> Where can i metus risus tincidunt?
									</a>
								</h4>
							</div>
							<div style="height: 0px;" id="collapseTwo-alt2" class="panel-collapse collapse">
								<div class="panel-body">
									Praesent tristique nisi in tortor placerat, non feugiat tellus pharetra. Mauris dignissim gravida odio, lobortis suscipit tellus egestas nec. Cras nunc odio, lacinia at feugiat ac, tincidunt ut ex. Donec dignissim sit amet velit sed pretium.							
								</div>
							</div>
						</div>
						<div class="panel faq-acc2">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion-alt2" href="#collapseThree-alt2">
									<i class="icon-help2"></i> Why the nec lectus sodales?
									</a>
								</h4>
							</div>
							<div style="height: 0px;" id="collapseThree-alt2" class="panel-collapse collapse">
								<div class="panel-body">
									Praesent tristique nisi in tortor placerat, non feugiat tellus pharetra. Mauris dignissim gravida odio, lobortis suscipit tellus egestas nec. Cras nunc odio, lacinia at feugiat ac, tincidunt ut ex. Donec dignissim sit amet velit sed pretium.							
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel-group" id="accordion-alt3">
						<div class="panel faq-acc3">
							<!-- Panel heading -->
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseOne-alt3">
									<i class="icon-help2" style="color:#000"></i> What are dignissim gravida odio?
									</a>
								</h4>
							</div>
							<div style="height: 0px;" id="collapseOne-alt3" class="panel-collapse collapse">
								<!-- Panel body -->
								<div class="panel-body">
									Praesent tristique nisi in tortor placerat, non feugiat tellus pharetra. Mauris dignissim gravida odio, lobortis suscipit tellus egestas nec. Cras nunc odio, lacinia at feugiat ac, tincidunt ut ex. Donec dignissim sit amet velit sed pretium.							
								</div>
							</div>
						</div>
						<div class="panel faq-acc3">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseTwo-alt3">
									<i class="icon-help2" style="color:#0FA2D5"></i> Where can i metus risus tincidunt?
									</a>
								</h4>
							</div>
							<div style="height: 0px;" id="collapseTwo-alt3" class="panel-collapse collapse">
								<div class="panel-body">
									Praesent tristique nisi in tortor placerat, non feugiat tellus pharetra. Mauris dignissim gravida odio, lobortis suscipit tellus egestas nec. Cras nunc odio, lacinia at feugiat ac, tincidunt ut ex. Donec dignissim sit amet velit sed pretium.							
								</div>
							</div>
						</div>
						<div class="panel faq-acc3">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseThree-alt3">
									<i class="icon-help2" style="color:#FF4862"></i> Why the nec lectus sodales?
									</a>
								</h4>
							</div>
							<div style="height: 0px;" id="collapseThree-alt3" class="panel-collapse collapse">
								<div class="panel-body">
									Praesent tristique nisi in tortor placerat, non feugiat tellus pharetra. Mauris dignissim gravida odio, lobortis suscipit tellus egestas nec. Cras nunc odio, lacinia at feugiat ac, tincidunt ut ex. Donec dignissim sit amet velit sed pretium.							
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix space60"></div>
			<div class="block-heading">
				<h3><span>FAQ Style - #3</span></h3>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4 faq-simple">
					<h3>1. What is lorem ipsum?</h3>
					<p>Cras eu interdum nunc. Morbi in lorem et elit  a mollis nibh. Duis dapibus odio sit amet sagittis ornare. Nulla hendrerit lacus imperdiet mauris efficitur vehicula.</p>
				</div>
				<div class="col-md-4 col-sm-4 faq-simple">
					<h3>2. Where do we use it?</h3>
					<p>Cras eu interdum nunc. Morbi in lorem et elit  a mollis nibh. Duis dapibus odio sit amet sagittis ornare. Nulla hendrerit lacus imperdiet mauris efficitur vehicula.</p>
				</div>
				<div class="col-md-4 col-sm-4 faq-simple">
					<h3>3. When we started using it?</h3>
					<p>Cras eu interdum nunc. Morbi in lorem et elit  a mollis nibh. Duis dapibus odio sit amet sagittis ornare. Nulla hendrerit lacus imperdiet mauris efficitur vehicula.</p>
				</div>
			</div>
		</div>
	</div>

	<?php include ('footer.php'); ?>
</div>



<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Plugins -->
<script src="js/bootstrap.min.js"></script>
<script src="js/menu.js"></script>
<script src="js/owl-carousel/owl.carousel.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/isotope/isotope.pkgd.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/tweecool.js"></script>
<script src="js/flexslider/jquery.flexslider.js"></script>
<script src="js/easypie/jquery.easypiechart.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.inview.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="js/main.js"></script>

</body>
</html>
