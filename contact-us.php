<!DOCTYPE html>
<html lang="en">
<head>
  
	<title>Contact Us | Soham Labels</title>
	
	<?php include ('head.php'); ?>

</head>
<body>

<div class="outer-wrapper">

	<!-- TOPBAR -->
	<?php include ('navbar.php'); ?>

	<!-- PAGE HEADER -->
	<div class="page_header">
		
		<div class="bcrumb-wrap">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul class="bcrumbs">
							<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
							<li>Contact</li>
						</ul>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- INNER CONTENT -->
	<div class="inner-content">
		<div class="container">
			<div class="row">
				<div class="col-md-8 margin10">
					<p style="font-size: 17px; color: black;">At Soham Labels, we are always ready to help our valuable customers by proving instant and quick support. You can always send your queries and valuable feedbacks to us by filling below form.</p>
					<div class="form-contact">
						<div class="required">
							<p>( <span style="color:red">*</span> fields are required )</p>
						</div>
						<form class="b-form b-contact-form contact-form" action="contact.php">
							<div class="row">
								<div class="col-md-6">
									<div class="row control-group">
										<div class="form-group col-xs-12 controls">
											<label>Name<span>*</span></label>
											<input class="field-name form-control input-lg"  type="text" placeholder="Name (required)">
											<p class="help-block"></p>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row control-group">
										<div class="form-group col-xs-12 controls">
											<label>Email Address<span>*</span></label>
											<input class="field-email form-control input-lg" type="text" placeholder="E-mail (required)">
											<p class="help-block"></p>
										</div>
									</div>
								</div>
							</div>
							<div class="row control-group">
								<div class="form-group col-xs-12  controls">
									<label>Phone Number<span>*</span></label>
									<input class="field-phone form-control" placeholder="Phone Number" type="tel">
									<p class="help-block"></p>
								</div>
							</div>
							<div class="row control-group">
								<div class="form-group col-xs-12 controls">
									<label>Message<span>*</span></label>
									<textarea rows="5" class="field-comments form-control input-lg" placeholder="Message"></textarea>
									<p class="help-block"></p>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="form-group col-xs-12">
									<button type="submit" class="btn-submit button btn-md">Send Message</button>
								</div>
							</div>
						</form>
					</div>
					<!--contact form-->
				</div>
				<div class="col-md-4">
					<div class="map-border">
						<div id="map-greyscale"></div>
					</div>
					<div class="space50"></div>
					<h3 class="no-margin">Contact info</h3>
					<div class="space20"></div>
					<ul class="contact-info">
						<li>
							<p><strong><i class="fa fa-map-marker"></i> Address:</strong> Plot No 69, A-2, Block No 59, Near Dairy Don Factory, Moti Naroli, Ta: Mangrol, Kim, Surat - 394110, Gujarat</p>
						</li>
						<li>
							<p><strong><i class="fa fa-envelope"></i> Mail Us:</strong> <a href="mailto:sohamlabels@gmail.com"> sohamlabels@gmail.com </a></p>
						</li>
						<li>
							<p><strong><i class="fa fa-phone"></i> Phone:</strong> +91 9601319607</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!-- TWEET / SUBSCRIBE -->
	

	<?php include ('footer.php'); ?>
</div>



<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Plugins -->
<script src="js/bootstrap.min.js"></script>
<script src="js/menu.js"></script>
<script src="js/owl-carousel/owl.carousel.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/isotope/isotope.pkgd.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/tweecool.js"></script>
<script src="js/flexslider/jquery.flexslider.js"></script>
<script src="js/easypie/jquery.easypiechart.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.inview.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="js/main.js"></script>

  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1Qy92yEPDj-avY2NT_sxT0l2FGAefr-g&callback=initMap"
  type="text/javascript"></script>
<script src="js/gmaps/greyscale.js"></script>


</body>
</html>
